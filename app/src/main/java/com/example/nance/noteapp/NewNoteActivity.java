package com.example.nance.noteapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class NewNoteActivity extends AppCompatActivity {
    EditText eText;
    EditText etitle;
    Button gray;
    Button white;
    Button green;
    Button normal;
    Button under;
    Button ita;
    Button bold;

    FileOutputStream outputStream;
    String filename;
    File file;
    RelativeLayout currentLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);

        eText=(EditText)findViewById(R.id.newNote);
        etitle=(EditText) findViewById(R.id.newTitle);
        currentLayout = (RelativeLayout) findViewById(R.id.newNoteLayout);

        gray=(Button)findViewById(R.id.Graybutton);
        gray.setBackgroundColor(Color.LTGRAY);
        gray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLayout.setBackgroundColor(Color.LTGRAY);
            }
        });

        white=(Button)findViewById(R.id.whiteButton);
        white.setBackgroundColor(Color.WHITE);
        white.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLayout.setBackgroundColor(Color.WHITE);
            }
        });

        green=(Button)findViewById(R.id.greenbutton);
        green.setBackgroundColor(Color.GREEN);
        green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLayout.setBackgroundColor(Color.GREEN);
            }
        });

        bold=(Button) findViewById(R.id.Bold);
        bold.setTypeface(null, Typeface.BOLD);
        bold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etitle.setTypeface(null, Typeface.BOLD);
                eText.setTypeface(null, Typeface.BOLD);
            }
        });

        normal=(Button) findViewById(R.id.Normalbutton);
        normal.setTypeface(null, Typeface.NORMAL);
        normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eText.setTypeface(null, Typeface.NORMAL);
                eText.setPaintFlags( eText.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
                etitle.setTypeface(null, Typeface.NORMAL);
                etitle.setPaintFlags( eText.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            }
        });
        ita=(Button) findViewById(R.id.Italicbutton);
        ita.setTypeface(null, Typeface.ITALIC);
        ita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eText.setTypeface(null, Typeface.ITALIC);
                etitle.setTypeface(null, Typeface.ITALIC);
            }
        });
        under=(Button) findViewById(R.id.undbutton);
        under.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        under.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eText.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                etitle.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.newnote_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.note_list:
                viewNoteList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void saveNote(View view){
        filename=etitle.getText().toString();
        file = new File(getFilesDir(), filename);

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(eText.getText().toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //set up toast
        Toast.makeText(
                NewNoteActivity.this,
                filename + " saved",
                Toast.LENGTH_LONG).show();

        eText.getText().clear();
        etitle.getText().clear();

    }

    public void viewNoteList(){
        Intent viewNoteListIntent=new Intent(this,NoteList.class);
        startActivity(viewNoteListIntent);
    }
}
