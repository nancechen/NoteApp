package com.example.nance.noteapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class NoteViewActivity extends AppCompatActivity {
    TextView titleView;
    TextView noteView;
    FileInputStream inputStream;
    File noteFile;
    String noteName;
    public final static String EXTRA_MESSAGE = "com.example.nance.noteapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_view);

        titleView = (TextView) findViewById(R.id.TitleView);
        noteView = (TextView) findViewById(R.id.NoteView);

        Intent viewIntent = getIntent();
        noteName = viewIntent.getStringExtra(NoteList.NOTE_TITLE);
        titleView.setText(noteName);

        noteFile = new File(getFilesDir(), noteName);

        if (noteFile.exists()) {
            try {
                inputStream = openFileInput(noteName);
                BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuilder buffer = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    buffer.append(line);
                }
                noteView.setText(buffer.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.noteview_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.new_note:
                newNote();
                return true;
            case R.id.note_list:
                showNoteList();
                return true;
            case R.id.edit_note:
                editNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void newNote(){
        Intent createNewNoteIntent= new Intent(this,NewNoteActivity.class);
        startActivity(createNewNoteIntent);
    }
    public void showNoteList(){
        Intent viewNoteListIntent=new Intent(this,NoteList.class);
        startActivity(viewNoteListIntent);

    }


    public void deleteNote(View view) {
        noteFile.delete();

        Toast.makeText(
                NoteViewActivity.this,
                noteName + " deleted",
                Toast.LENGTH_LONG).show();
        //back to note list page
        Intent viewNoteListIntent=new Intent(this,NoteList.class);
        startActivity(viewNoteListIntent);


    }

    public void editNote() {
        Intent editNoteIntent = new Intent(getApplicationContext(), NoteEdit.class);
        editNoteIntent.putExtra(EXTRA_MESSAGE,noteName);
        startActivity(editNoteIntent);


    }


}
