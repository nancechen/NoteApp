package com.example.nance.noteapp;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;



public class NoteList extends AppCompatActivity {



    String[] SavedFiles;
    ListView listSavedFiles;
    SearchView searchNotesView;
    ArrayAdapter<String> adapter;
    public final static String NOTE_TITLE = "com.example.nance.noteapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);
        listSavedFiles = (ListView) findViewById(R.id.NoteList);
        searchNotesView = (SearchView) findViewById(R.id.searchView);



        SavedFiles = getApplicationContext().fileList();
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,SavedFiles);

        listSavedFiles.setAdapter(adapter);
        listSavedFiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String clickedFileName = (String) listSavedFiles.getItemAtPosition(position);

                Intent viewNoteIntent = new Intent(getApplicationContext(), NoteViewActivity.class);
                viewNoteIntent.putExtra(NOTE_TITLE, clickedFileName);
                startActivity(viewNoteIntent);


            }
        });
        searchNotesView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);


                return false;
            }
        });

    }



}
