package com.example.nance.noteapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class NoteEdit extends AppCompatActivity {
    EditText eTitle;
    EditText eNote;
    FileOutputStream outputStream;
    FileInputStream inputStream;
    String filename;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_edit);
        eTitle=(EditText)findViewById(R.id.editTitle);
        eNote=(EditText) findViewById(R.id.editNote);

        Intent editIntent = getIntent();
        filename= editIntent.getStringExtra(NoteViewActivity.EXTRA_MESSAGE);
        eTitle.setText(filename);

        file = new File(getFilesDir(), filename);

        if (file.exists()) {
            try {
                inputStream = openFileInput(filename);
                BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuilder buffer = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    buffer.append(line);
                }
                eNote.setText(buffer.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void saveNote(View view){
        filename=eTitle.getText().toString();
        file = new File(getFilesDir(), filename);

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(eNote.getText().toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //set up toast
        Toast.makeText(
                NoteEdit.this,
                filename + " saved",
                Toast.LENGTH_LONG).show();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.editnote_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.note_list:
                viewNoteList();
                return true;
            case R.id.edit_undo:
                undoChange();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void viewNoteList(){
        Intent viewNoteListIntent=new Intent(this,NoteList.class);
        startActivity(viewNoteListIntent);
    }

    public void undoChange(){
        file = new File(getFilesDir(), filename);

        if (file.exists()) {
            try {
                inputStream = openFileInput(filename);
                BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuilder buffer = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    buffer.append(line);
                }
                eNote.setText(buffer.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
